/*
 * DA4
 *
 * Created: 2/25/2017 2:29:09 PM
 * Author : doriaj3
 */ 
#define F_CPU 8000000L
#include <avr/io.h>
#include <util/delay.h>

int main(void) {

   //Configure TIMER1
   TCCR1A|=(1<<COM1A1)|(1<<COM1B1)|(1<<WGM11);        //NON Inverted PWM
   TCCR1B|=(1<<WGM13)|(1<<WGM12)|(1<<CS11)|(1<<CS10); //PRESCALER=64 MODE 14(FAST PWM)
   ICR1 = 2499; //TOP

//----------------------------------		
	DDRB = 0xFF; // make Port B an output
	DDRD = 0xFF; // make Port D an output
	DDRC = 0x0; // make Port C an input (for ADC input)
	DIDR0 = 0x1; // disable digital input on ADC0 pin
	ADMUX = 0x0; // Reference = Aref, ADC0 (PC.0) used as analog input
	// data is right-justified
	ADCSRA = 0x87; // enable ADC, system clock used for A/D conversion
	ADCSRB = 0x0; // free running mode
	
	while (1) {
		ADCSRA |= (1 << ADSC); // start conversion
		while ( (ADCSRA&(1<<ADIF)) == 0 ) ; // wait for conversion to finish
			

OCR1B =ADC*.3125;


	}
	
	return 0;
}