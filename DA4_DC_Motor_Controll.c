/*
 * DA4 DC Motor
 *
 * Created: 2/25/2017 2:29:09 PM
 * Author : doriaj3
 */ 
#define F_CPU 8000000L
#include <avr/io.h>
#include <util/delay.h>

int main(void) {

	// PD.6 (OC0A) is an output
	 DDRD |= (1 << 6);
	 // PWM with 50% duty cycle
	 OCR0A = 0;
	 // non-inverting mode
	 TCCR0A |= (1 << COM0A1);
	 // fast PWM mode
	 TCCR0A |= (1 << WGM01) | (1 <<WGM00);
	 // set prescaler to 8 and enable Timer0
	 TCCR0B |= (1 << CS01);
	 
//----------------------------------		
	DDRB = 0xFF; // make Port B an output
	DDRD = 0xFF; // make Port D an output
	DDRC = 0x0; // make Port C an input (for ADC input)
	DIDR0 = 0x1; // disable digital input on ADC0 pin
	ADMUX = 0x0; // Reference = Aref, ADC0 (PC.0) used as analog input
	// data is right-justified
	ADCSRA = 0x87; // enable ADC, system clock used for A/D conversion
	ADCSRB = 0x0; // free running mode
	
	while (1) {
		ADCSRA |= (1 << ADSC); // start conversion
		while ( (ADCSRA&(1<<ADIF)) == 0 ) ; // wait for conversion to finish
		//PORTD = ADCL; // send low byte to PORTD
		//PORTB = ADCH; // send high byte to PORTB
				
		if(ADC > 512){
		//	_delay_ms(500);
			PORTB = 0x01;	//hbridge.forward
			OCR0A  = ADC/4;

		}
		else{	
		//	_delay_ms(500);		
			PORTB = 0x02; // hbridge.backward
			OCR0A  = 255-(ADC/4);	
							
		}	
	}
	return 0;
}