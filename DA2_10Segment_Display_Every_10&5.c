/*
 * DA2_3
 *
 * Created: 2/11/2017 9:40:18 PM
 * Author : doriaj3
 */ 

#include <avr/io.h>

int main(void) {
	volatile unsigned int fcnt = 0;		//Count for every five rising edge
	volatile unsigned int tcnt = 0;		//count for every tenth rising edge
	volatile unsigned int chkHi = 0;	//checks if it is a rising edge
	volatile unsigned int cntHi = 0;	//count for every rising edge
	volatile unsigned char cnt = 0;		// initialize cnt to keep track of number of 
										
										// times Timer0 overflows
	DDRC |= (1 << 0);					// connect LED to pin PB.5
	DDRC |= (1 << 4);					// Configure pins
	DDRC |= (1 << 5);
	DDRB = 0xFF;
	PORTC = 0;							// PC.0 LED is off

	while (1) {
										// set up Timer0 with prescaler = 1024 and normal mode
	  TCCR0A = 0;
	  TCCR0B |= (1 << CS02)|(1 << CS00);
	  TCNT0 = 0;						// initialize counter

 	  while( (TIFR0 & 0x1) == 0 ) ;		// wait until overflow flag is set
	  TCCR0B = 0;						// stop/disable Timer 0
	  TIFR0 |= 1;						// clear overflow flag
	  if (cnt == 6) {

for (volatile unsigned int i = 0; i<36; i++){
for (volatile unsigned int j = 0; j < 255; j++);
}
		PORTC ^= 0x01;					// toggle PB.5
		cnt = 0;						// reset cnt
		chkHi ^= 1;

	if (chkHi == 1){					//If it is a rising edge it check how many pulses have occurred
		cntHi++;						//increment the amount of times it is a rising edge
		fcnt++;							//if this count becomes 5, it will know that 5 pulses have occurred
		tcnt++;							//if this count becomes 10, it will know that 10 pulses have occurred
	
	if(fcnt == 5 && tcnt == 10){
	PINC = 0x20;						// toggle pin 5
	PINC = 0x10;						// toggle pin 4
	fcnt = 0;							// reset the counts back to 0
	tcnt = 0;							// we toggle both pins because it makes sure every 5th pulse toggles and every 10th toggles accordingly
	}
else 
	if (fcnt == 5){						// this statement will only execute if we've only had 5 pulses
		PINC = 0x20;					// toggle the 5th pin
		fcnt = 0;						// reset the pin
	}

		PORTB = cntHi;

		}								//if(chckHi==1)


	  }
	  else
		cnt++;							// increment cnt
   }
}

