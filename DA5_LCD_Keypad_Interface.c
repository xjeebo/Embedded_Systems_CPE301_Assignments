/*
 * DA5.c
 * Author: doriaj3
 * Class: CPE 301
 */ 

#define F_CPU 8000000L
#include <avr/io.h>
#include <util/delay.h>

// Connect LCD data pins to PORTB and control pins to PORTC
// RS = PC.0
// RW = PC.1
// EN = PC.2
#define LCD_DPRT PORTB //LCD DATA PORT 
#define LCD_DDDR DDRB  //LCD DATA DDR 
#define LCD_DPIN PINB  //LCD DATA PIN  
#define LCD_CPRT PORTC //LCD COMMANDS PORT 
#define LCD_CDDR DDRC  //LCD COMMANDS DDR 
#define LCD_CPIN PINC  //LCD COMMANDS PIN 
#define LCD_RS  0      //LCD RS   (PC.0)
#define LCD_RW  1      //LCD RW   (PC.1)
#define LCD_EN  2      //LCD EN   (PC.2)

#define KEY_PRT PORTD  //keyboard PORT
#define KEY_DDR DDRD   //keyboard DDR
#define KEY_PIN PIND   //keyboard PIN

unsigned char keypad[ 4][ 4] ={ {'1','2','3','u'},
								{'4','5','6','d'},
								{'7','8','9','s'},
								{' ','0',' ','e'},
												};
int n = 0;			// lcd data count so we can switch over 
					// from the left 8spaces to the right 8 spaces on the LCD
//**************************************************************** 
void lcdCommand (unsigned char cmd) {  
	LCD_DPRT = cmd;				//send cmd to data port  
	LCD_CPRT &= ~(1<<LCD_RS);   //RS = 0 for command  
	LCD_CPRT &= ~(1<<LCD_RW);   //RW = 0 for write   
	LCD_CPRT |= (1<<LCD_EN);    //EN = 1 for H-to-L pulse  
	_delay_us(1);				//wait to make enable wide  
	LCD_CPRT &= ~(1<<LCD_EN);   //EN = 0 for H-to_L pulse  
	_delay_us(100);				//wait to make enable wide 
} 
 
void lcdData(unsigned char data) {  
	n++;
	if(n==9)
		lcd_gotoxy(1,2);
	if(n==17){
		n = 0;
		lcd_gotoxy(1,1);
	lcd_init();
	}
	LCD_DPRT = data;				//send data to data port  
	LCD_CPRT |= (1<<LCD_RS);		//RS = 1 for data  
	LCD_CPRT &= ~(1<<LCD_RW);	    //RW = 0 for write   
	LCD_CPRT |= (1<<LCD_EN);		//EN = 1 for H-to-L pulse  
	_delay_us(1);					//wait to make enable wide  
	LCD_CPRT &= ~(1<<LCD_EN);		//EN = 0 for H-to_L pulse  
	_delay_us(100);					//wait to make enable wide  
	
} 

void lcd_init() {  

	LCD_DDDR = 0xFF;  
	LCD_CDDR = 0xFF;    
	LCD_CPRT &=~(1<<LCD_EN);		//LCD_EN = 0  
	_delay_us(2000);    
	//wait for init  
	lcdCommand(0x38);				//initialize LCD 2 line, 5x7  
	lcdCommand(0x0E);				//display on, cursor on  
	lcdCommand(0x01);				//clear LCD  
	_delay_us(2000);				//wait  
	lcdCommand(0x06);				//shift cursor right 
} 

void lcd_print(char * str) {		// Prints out the string character by character
	unsigned char i = 0;			// count to traverse the char array 
	
	while (str[i]!=0){
		if(i==8){
			
			lcdCommand(0xC0);		// Since our LCD is an 8x2 we need to 
									// Relocate from the left 8spots to the right 8
		}
		if(i==16)
			lcdCommand(0x01);		// clear LCD because anything over 16 characters 
									// doesn't fit, but it will continue to write the rest.
		
		lcdData(str[i]); i++;		// Prints out the character
	}
} 

void lcd_gotoxy(unsigned int x, unsigned int y) {   // go to specific LCD locations
	unsigned char firstCharAdr[] = {0x80, 0xC0};    // locations of the first character of each line
	lcdCommand(firstCharAdr[y-1] + x-1);  
	_delay_us(100); 
} 

int main(void){
	unsigned int cpass = 0;			// a count that increments if the correct passcode digit is pressed 
	unsigned char colloc, rowloc;	// rows and collumn variables
	
	lcd_init();						// initialize the LCD	
	
									//keyboard routine. This sends the ASCII
									//code for pressed key to port d
	KEY_DDR = 0xF0;				    // configure keypad ports, Rows are outputs and columns are inputs
	KEY_PRT = 0xFF;				    // Enable all pullups

	while(1){
		do{
			KEY_PRT &= 0x0F;		    // ground all rows at once, KEY_PRT = 0b0000FFFF; rows are the left 4 bits
			colloc = (KEY_PIN & 0x0F);	// read the coloumns, which is done by &ing them because if KEY_PIN has a value other than 0x0F it
		}								// adjust collocs value
		while (colloc != 0x0F);			// check until all keys released, as long as it is 0x0F, no change to KEY_PIN has occurred.
		do{
			do{
										// Pin 1-4 on the 4x4 Keypad is connected to PIND4-7, which are columns. KEY_PIN is our PIND's
										// 0bxxxx1110 & 0b00001111 = 0b00001110  
										// 0bxxxx1101 & 0b00001111 = 0b00001101
										// 0bxxxx1011 & 0b00001111 = 0b00001011
										// 0bxxxx0111 & 0b00001111 = 0b00000111
	 	
			_delay_ms(20);		    	// call delay
			colloc = (KEY_PIN & 0x0F);  // see if any key is pressed
			}
			 while (colloc == 0x0F);	//keep checking for key press
			_delay_ms(20);				//call delay for debounce
			colloc = (KEY_PIN & 0x0F);  //read columns
		}
			
		while (colloc == 0x0F);		    //wait for key press

		while(1){
			KEY_PRT = 0xEF;				//ground row 0 0b11101111, the first row since rows are the left 4 bits.
			_delay_ms(10);
			colloc = (KEY_PIN & 0x0F);  //read the columns
			if (colloc != 0x0F)			//column detected
			{
				rowloc = 0;				//save row location
				break;					//exit while loop
			}
			KEY_PRT = 0xDF;				//ground row 1
			_delay_ms(10);
			colloc = (KEY_PIN & 0x0F);  //read the columns
			if(colloc != 0x0F)			//column detected
			{
				rowloc = 1;			    //save row location
				break;					//exit while loop
			}
			KEY_PRT = 0xBF;				//ground row 2
			_delay_ms(10);
			colloc = (KEY_PIN & 0x0F);  //read the columns
			if(colloc != 0x0F)		    //column detected
			{
				rowloc = 2;			    // save row location
				break;					// exit while loop
			}

			KEY_PRT = 0x7F;				// ground row 3
			_delay_ms(10);
			colloc = (KEY_PIN & 0x0F);  // read the columns
			rowloc = 3;				    // save row location
			break;						// exit while loop
		}
										// check column and send result to Port D
										
			if(colloc == 0x0E){			// collumn 0, 0bXXXX1110
			
			if(rowloc == 2 && colloc == 0x0E && cpass == 2)	 // If the third keypad is correctly pressed it will 
			cpass++;										 // increment the cpass count

			lcdData(keypad[rowloc][0]);						 // Prints out the key pressed on to the LCD
	
		if(rowloc == 3)										 // clear screen by
			lcd_init();										 // reinitializing the screen 
		}
		else if(colloc == 0x0D){							 // column 1, 0bXXXX1101
			
			lcdData(keypad[rowloc][1]);						 // print out the key pressed on LCD
				//------------------------------------------ //cpass is going to keep track if our passcode is correct
			if(rowloc == 1 && cpass == 0)					 // if the first number hit is 5 it increments cpass
				cpass++;									 

			if(rowloc == 0 && cpass == 1)					 // the correct number was pressed after the first correct 5 then 2
				cpass++;
				
		}
		else if(colloc == 0x0B){	 // column 2, 0bXXXX1011
			   
			if(rowloc == 3){		 // help

				lcd_gotoxy(1,1);	 // first 8 spots on LCD 
				n = 0;				 // reset the char counts
				lcdCommand(0x01);	 // clears LCD
				_delay_ms(100);		 // delay so it prints out Help?....
				lcd_print("Help?...");
				_delay_ms(1000);	 // delay 
				n = 0;
				lcd_gotoxy(1,2);
				lcd_print("Too Bad");
				_delay_ms(1000);
				lcd_gotoxy(1,1);
				lcdCommand(0x01);   //clear LCD  
				lcd_gotoxy(1,1);
				n=0;
					  }
			lcdData(keypad[rowloc][2]);						
		}
		else
			lcdData(keypad[rowloc][3]);

			if(cpass == 3){			// when cpass = 3, it will print system unlocked.

				lcd_gotoxy(1,1);
				lcd_print("                ");
				lcd_gotoxy(1,1);
				n = 0;
				lcd_print("System Unlocked");
				lcd_gotoxy(1,1);
				cpass = 0; 
				_delay_ms(2000);
				lcd_init();
				n = 0;
								}
	}

	return 0;
}   



 