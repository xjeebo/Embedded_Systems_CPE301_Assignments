/*
 * DA6.c
 *
 * Created: 3/12/2017 2:26:47 PM
 * Author : doriaj3
 */ 

#include <avr/io.h>
#define F_CPU 8000000	// Clock Speed
#include <util/delay.h>
#include <stdio.h>
#define BAUD  9600

void initUART();
void writeChar(unsigned char c);
void writeSpace();


int main( void ){
	
	DDRC = 0x0;		   // make Port C an input (for ADC input)
	DIDR0 = 0x1;	  // disable digital input on ADC0 pin
	ADMUX = 0x0;	 // Reference = Aref, ADC0 (PC.0) used as analog input
					// data is right-justified
	ADCSRA = 0x87; // enable ADC, system clock used for A/D conversion
	ADCSRB = 0x0; // free running mode
	
	char ch[4];  // Initialize array of characters to null
	initUART();							 // Initualize UART

	while (1) {
		ADCSRA |= (1 << ADSC);				 // start conversion
		while ( (ADCSRA&(1<<ADIF)) == 0 ) ; // wait for conversion to finish
		
		sprintf(ch,"%d",ADC);			  // each loop refills the array of the converted integers to characters
	
	for(int i = 0; i<=4; i++){			// display the characters in the array 
		writeChar(ch[i]);
	}

		writeSpace();				// a function that just writes 1 space.
	}
	
	return 0;
}

void initUART() {
	unsigned int baudrate;

	// Set baud rate:  UBRR = [F_CPU/(16*BAUD)] -1
	baudrate = ((F_CPU/16)/BAUD) - 1;
	UBRR0H = (unsigned char) (baudrate >> 8);
	UBRR0L = (unsigned char) baudrate;

	UCSR0B |= (1 << RXEN0) | (1 << TXEN0);
	// Enable receiver and transmitter
	UCSR0C |= (1 << UCSZ01) | (1 << UCSZ00);		// Set data frame: 8 data bits, 1 stop bit, no parity
}

void writeChar(unsigned char c) {
	UDR0 = c;									// Display character on serial (i.e., PuTTY) terminal
	_delay_ms(5);
}

void writeSpace(){							// writes 1 space on the terminal
	UDR0 = ' ';
	_delay_ms(1000);
}